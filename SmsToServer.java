package com.cloud.smsinlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SmsToServer extends Activity implements OnClickListener {
	
	public Map<String,Sms> texts = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dump_layout);
		TextView sd = (TextView) findViewById(R.id.SMS_DUMP);
		Button dump = (Button) findViewById(R.id.dump_sms);
		
			//check if am connected or not 
		if(isConnected()){
			sd.setBackgroundColor(0xFF00CC00);
			sd.setText("Internet Connected!");
		}else{
			sd.setText("No Internet Connection");
		}
		
		//add click listener to BUTTON DUMPSMS.
		dump.setOnClickListener(this);
		
	}
	
	//get all sms's from Android phone and return them as MAP.
	public Map<String,Sms> getAllSms(){
		Log.e("apps", "hapahapa");
		Map<String,Sms> lstSms = new HashMap<String, Sms>();
		Sms objSms = new Sms();
		Uri message = Uri.parse("content://sms/");
		ContentResolver cr = getApplicationContext().getContentResolver();
		
		Cursor c = cr.query(message, null, null, null, null);
		int totalSMS = c.getCount();
		Log.e("apps else", totalSMS+ "total");
		
		if(c.moveToFirst()){
			for(int i=0; i<totalSMS; i++){
				objSms = new Sms();
				objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
				objSms.setAddress(c.getString(c.getColumnIndexOrThrow("address")));
				objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
				objSms.setReadState(c.getString(c.getColumnIndexOrThrow("read")));
				objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
				
				if(c.getString(c.getColumnIndexOrThrow("type")).contains("1")){
					objSms.setFolderName("inbox");
				} else{
					objSms.setFolderName("sent");
				}
			
			lstSms.put("AllSMS", objSms);
			Log.d("lstSms", lstSms+"ALLSMS");
			c.moveToNext();
			}
		}
		
		c.close();
		
		return lstSms;
		
	}
	
    public static String POST(String url,Map<String, Sms> apps ){
    	Sms wekasms1 = new Sms();
		InputStream ip = null;
		String result = "";
		
		try{
			//create http client 
			HttpClient httpclient = new DefaultHttpClient();
			
			//make POST request to the given URL. 
			HttpPost httpPost = new HttpPost(url);
			
			String json = "";
			
			//build jsonObject
			JSONObject jsonObject = new JSONObject();
									
			jsonObject.put("id", wekasms1.getId());
			jsonObject.put("address", wekasms1.getAddress());
			jsonObject.put("read", wekasms1.getReadState());
			jsonObject.put("body", wekasms1.getMsg());
			jsonObject.put("date", wekasms1.getTime());
			jsonObject.put("FolderName", wekasms1.getFolderName());
			
			
			Log.d("jsonOb", jsonObject+"built jsonObject");
				
			//convert JSONObject to JSON to String 
			json = jsonObject.toString();
			Log.e("converted jsonObj to String", json);
			
			//**Alternative method of converting Sms object to Json
			/*ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(hizosms);
			*/
			
			//set json to string entity
			StringEntity se = new StringEntity(json);
			
			//set httpPost Entity
			httpPost.setEntity(se);
			Log.d("httpPost", httpPost+ "We've set httpPost Entity");
			//Set some headers to inform server about the type of the content.
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			
			//Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);
			
			//recieve response as inputstream
			ip = httpResponse.getEntity().getContent();
			
			//convert inputstream to string
			if(ip!=null){
				result = convertInputStreamToString(ip);
			}
		}catch(Exception e){
			Log.d("InputStream", e.getLocalizedMessage());
		}
		//return result
		return result;
		
	}
	
		public boolean isConnected(){
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if(networkInfo != null && networkInfo.isConnected())
			return true;
		else
			return false;
			
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.dump_sms:
			  
			new HttpAsyncTask().execute("insert link to server");
		}
	}
	
	private static String convertInputStreamToString(InputStream ip) throws IOException {
		BufferedReader bfr =  new BufferedReader(new InputStreamReader(ip));
		String line = "";
		String result = "";
		
		while((line = bfr.readLine()) !=null)
			result += line;
		ip.close();
		
		return result;
	}
	
	class HttpAsyncTask extends AsyncTask<String, Void, String>{
		
		

		@Override
		protected String doInBackground(String... urls) {
			
			Map<String, Sms> apps = getAllSms();
			
			final int max = apps.size();
						
			for(int i=0; i<max; i++){
				apps.get(i);
			}
			
			
			return POST(urls[0],apps);
			// TODO Auto-generated method stub
							
		}

		//display the result of the Asynctask
		@Override
		protected void onPostExecute(String result) {
		Toast.makeText(getBaseContext(), "ALLSMS SENT", Toast.LENGTH_LONG).show();
	}
}
	
}
	

	
	
